import 'package:flutter/material.dart';
import 'package:testproject/screen/secondPage.dart';
import 'package:testproject/model/userModel.dart';
import 'package:testproject/service/userService.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<User> _users;
  bool _loading;

  @override
  void initState() {
    super.initState();
    _loading = true;
    Services.getUsers().then((users) {
      setState(() {
        _users = users;
        _loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(_loading ? 'Loading...' : 'Users')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: null == _users ? 0 : _users.length,
                itemBuilder: (contxt, index) {
                  return GestureDetector(
                    child: ListTile(
                      title: Text(_users[index].title),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SecondPage()),
                      );
                    },
                  );
                })
          ],
        ),
      ),
    );
  }
}
